package Test;

import org.junit.runner.RunWith;
import org.testng.annotations.Test;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(Cucumber.class)

@CucumberOptions(
		features = "src/main/java/aFeatures/",
		plugin = "pretty",
		monochrome = true, 
		snippets = SnippetType.CAMELCASE,
		dryRun = false,
		strict = true,
		glue = {""}, 
		tags = { "@calculaValores"}
)

@Test(priority = 11)

public class CalculadoraTest extends AbstractTestNGCucumberTests {
}
