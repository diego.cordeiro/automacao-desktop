package Pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import Elements.CalculadoraElements;
import Setups.TestRule;
import Utils.ReportUtils;
import Utils.WiniumUtils;

public class CalculadoraPage extends CalculadoraElements{
	
	public CalculadoraPage(){
		driver = TestRule.getDriver();
		PageFactory.initElements(TestRule.getDriver(), this);
	}
		
	public void pressionarTecla(String tecla) throws InterruptedException {
		if(tecla.equals("0")){
			BOTAO_0.click();
		}else if (tecla.equals("1")) {
			BOTAO_1.click();
		}else if (tecla.equals("2")) {
			BOTAO_2.click();
		}else if (tecla.equals("3")) {
			BOTAO_3.click();
		}else if (tecla.equals("4")) {
			BOTAO_4.click();
		}else if (tecla.equals("5")) {
			BOTAO_5.click();
		}else if (tecla.equals("6")) {
			BOTAO_6.click();
		}else if (tecla.equals("7")) {
			BOTAO_7.click();
		}else if (tecla.equals("8")) {
			BOTAO_8.click();
		}else if (tecla.equals("9")) {
			BOTAO_9.click();
		}else if (tecla.equals(",")) {
			BOTAO_SEPARADOR_DECIMAIS.click();
		}else if (tecla.equals("+")) {
			BOTAO_OPERACAO_ADICAO.click();
		}else if (tecla.equals("-")) {
			BOTAO_OPERACAO_SUBTRACAO.click();
		}else if (tecla.equals("*")) {
			BOTAO_OPERACAO_MULTIPLICACAO.click();
		}else if (tecla.equals("/")) {
			BOTAO_OPERACAO_DIVISAO.click();
		}else if (tecla.equals("=")) {
			BOTAO_IGUAL_A.click();
		}
	}
	
	public void preencherValor(String valor){
		CAMPO_TXT_VALOR.click();
		CAMPO_TXT_VALOR.sendKeys(valor);
	}
	
	public boolean validarCalculoValoresDigitados(List<String> valores) throws Exception{
		Boolean retorno = false;
		String resultadoTela = CAMPO_TXT_VALOR.getAttribute("Name").replace("Exibição é ", "").replace(".", "").trim();
		String valor1 = valores.get(0).replace(",", ".");
		String operador =  valores.get(1);
		String valor2 = valores.get(2).replace(",", ".");
		String resultado = null; 
		
		switch (operador) {
		case "+":
			resultado = String.valueOf(Double.parseDouble(valor1) + Double.parseDouble(valor2));
			break;
		case "-":
			resultado = String.valueOf(Double.parseDouble(valor1) - Double.parseDouble(valor2));
			break;
		case "/":
			resultado = String.valueOf(Double.parseDouble(valor1) / Double.parseDouble(valor2));
			break;
		case "*":
			resultado = String.valueOf(Double.parseDouble(valor1) * Double.parseDouble(valor2));
			break;
		default:
			System.out.println("Operador desconhecido");
			retorno = false;
			break;
		}
		
		resultado = resultado.replace(".0", "").replace(".", ",");
		
		if(resultado.equals(resultadoTela)){
			retorno = true;
		}else {
			ReportUtils.logMensagem(Status.ERROR, "Erro! Os valores não conferem, favor verificar." +"<br>" 
					+ "Valor esperado: "+ resultado + "<br>"
					+ "Valor Obitido: " + resultadoTela + "<br>", 
					WiniumUtils.capturaTelaParaRelatorio());
		}
		
		return retorno;
	}
}
