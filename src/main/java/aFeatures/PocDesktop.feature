#language:pt
Funcionalidade: Fazer Cálculo na calculadora do windows

  @calculaValores
  Esquema do Cenario: Poc para acessar o calculadora do windows e efetuar um cálculo pelo teclado
    Dado que eu digite o valor <primeiro_valor>
    E que eu pressione a tecla <operacao>
    E que eu digite o valor <segundo_valor>
    Quando a tecla <igualdade> for pressionada
    Entao valido o resultado

    Exemplos: 
      | primeiro_valor | operacao | segundo_valor | igualdade |
      | "12"           | "+"      | "2"           | "="       |
      | "22"           | "-"      | "13"          | "="       |
      | "32"           | "*"      | "14"          | "="       |
      | "42"           | "/"      | "15"          | "="       |

