package Steps;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;

import com.aventstack.extentreports.Status;

import Pages.CalculadoraPage;
import Utils.ReportUtils;
import Utils.WiniumUtils;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CalculadoraSteps{

	CalculadoraPage page = new CalculadoraPage();
	List<String> valoresDigitados = new ArrayList<String>();
	
	@Dado("^que eu pressione a tecla \"([^\"]*)\"$")
	public void queEuPressioneATecla(String tecla) throws Throwable {
		valoresDigitados.add(tecla);
		page.pressionarTecla(tecla);
		ReportUtils.logMensagem(Status.INFO, "Dado que eu pressione a tecla " + tecla, WiniumUtils.capturaTelaParaRelatorio());
	}
	
	@Dado("^que eu digite o valor \"([^\"]*)\"$")
	public void queEuDigiteOValor(String valor) throws Throwable {
		valoresDigitados.add(valor);
		page.preencherValor(valor);
		ReportUtils.logMensagem(Status.INFO, "Dado que eu pressione a tecla " + valor, WiniumUtils.capturaTelaParaRelatorio());
	}
	
	@Quando("^a tecla \"([^\"]*)\" for pressionada$")
	public void ATeclaForPressionada(String tecla) throws Throwable {
		valoresDigitados.add(tecla);
		page.pressionarTecla(tecla);
		ReportUtils.logMensagem(Status.INFO, "Quando a tecla " + tecla + " for pressionada", WiniumUtils.capturaTelaParaRelatorio());
	}

	@Entao("^valido o resultado$")
	public void valido_o_resultado() throws Throwable {
		ReportUtils.logMensagem(Status.INFO, "Então valido se o cálculo está correto");
		Assert.assertTrue(page.validarCalculoValoresDigitados(valoresDigitados), "Erro! Os valores não conferem, favor verificar.");		
	}
	
}
