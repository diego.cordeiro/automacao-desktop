package Setups;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.openqa.selenium.winium.WiniumDriverService;

import com.aventstack.extentreports.Status;

import Utils.ReportUtils;
import Utils.WiniumUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class TestRule {

	public static String nomeCenario = null;
	public static WiniumDriver driver = null;
	public static WiniumDriverService service = null;
	public static DesktopOptions option = new DesktopOptions();
	public static WiniumUtils winiumUtils = new WiniumUtils();
	public static String caminhoDriverWinium = WiniumUtils.setValorArquivoProperties("winium.properties", "WINIUM_PATH_DRIVER");
	public static String urlDriverRemoteWinium = WiniumUtils.setValorArquivoProperties("winium.properties", "WINIUM_REMOTE_URL");
	public static String caminhoPrograma = WiniumUtils.setValorArquivoProperties("ambientes.properties", "PROGRAMA_PATH");

	@Before
	public void beforeCenario(Scenario cenario) throws SQLException, InterruptedException, IOException {
		ReportUtils.criarReport(cenario);

		ReportUtils.logMensagem(Status.INFO, "Iniciando instâncias");

		WiniumUtils.finalizaProcesso(WiniumUtils.setValorArquivoProperties("winium.properties", "WINIUM_PROCESSO"));

		option.setApplicationPath(caminhoPrograma);

		System.setProperty("webdriver.winium.desktop.driver", caminhoDriverWinium);

		service = new WiniumDriverService.Builder().usingDriverExecutable(new File(caminhoDriverWinium)).usingPort(9999)
				.withVerbose(true).withSilent(true).buildDesktopService();

		try {
			service.start();
			driver = new WiniumDriver(new URL(urlDriverRemoteWinium), option);
			Thread.sleep(1000);
			nomeCenario = cenario.getName();
			ReportUtils.logMensagem(Status.INFO, "Cenário: " + nomeCenario);

		} catch (IOException e) {
			System.out.println("Exception while starting WINIUM service");
			e.printStackTrace();
		}
	}

	public static WiniumDriver getDriver() {
		return driver;
	}

	public static String getNomeCensario() {
		return nomeCenario;
	}

	@After
	public void tearDown(Scenario cenario) throws InterruptedException, IOException {
		ReportUtils.atualizaReport(cenario);
		ReportUtils.logMensagem(Status.INFO, "Finalizando instâncias");
		WiniumUtils.finalizaProcesso(WiniumUtils.setValorArquivoProperties("ambientes.properties", "PROGRAMA_PROCESSO"));
		WiniumUtils.finalizaProcesso(WiniumUtils.setValorArquivoProperties("winium.properties", "WINIUM_PROCESSO"));
	}
}
