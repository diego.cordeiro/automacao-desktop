package Elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Utils.WiniumUtils;

public class CalculadoraElements extends WiniumUtils{

	
	@FindBy(id="num0Button")
	public WebElement BOTAO_0;
	
	@FindBy(id="num1Button")
	public WebElement BOTAO_1;
	
	@FindBy(id="num2Button")
	public WebElement BOTAO_2;
	
	@FindBy(id="num3Button")
	public WebElement BOTAO_3;
	
	@FindBy(id="num4Button")
	public WebElement BOTAO_4;
	
	@FindBy(id="num5Button")
	public WebElement BOTAO_5;
	
	@FindBy(id="num6Button")
	public WebElement BOTAO_6;
	
	@FindBy(id="num7Button")
	public WebElement BOTAO_7;
	
	@FindBy(id="num8Button")
	public WebElement BOTAO_8;
	
	@FindBy(id="num9Button")
	public WebElement BOTAO_9;
	
	@FindBy(id="decimalSeparatorButton")
	public WebElement BOTAO_SEPARADOR_DECIMAIS;
	
	@FindBy(id="plusButton")
	public WebElement BOTAO_OPERACAO_ADICAO;
	
	@FindBy(id="minusButton")
	public WebElement BOTAO_OPERACAO_SUBTRACAO;
	
	@FindBy(id="multiplyButton")
	public WebElement BOTAO_OPERACAO_MULTIPLICACAO;
	
	@FindBy(id="divideButton")
	public WebElement BOTAO_OPERACAO_DIVISAO;
	
	@FindBy(id="equalButton")
	public WebElement BOTAO_IGUAL_A;
	
	@FindBy(id="CalculatorResults")
	public WebElement CAMPO_TXT_VALOR;
}
