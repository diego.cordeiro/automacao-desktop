package Elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Utils.WiniumUtils;

public class CommonElements extends WiniumUtils{
	
	@FindBy(id="Minimize")
	public WebElement BOTAO_MINIMIZAR;
	
	@FindBy(id="Restore")
	public WebElement BOTAO_MAXIMIZAR;
	
	@FindBy(id="Close")
	public WebElement BOTAO_FECHAR;
	
}
