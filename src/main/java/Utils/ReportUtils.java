package Utils;

import java.io.File;
import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import cucumber.api.Scenario;

public class ReportUtils extends WiniumUtils {

	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extentReport;
	public static ExtentTest extentTest;
	public static String diretorioReport;

	public static void criarReport(Scenario cenario) {
		if (extentReport == null) {
			extentReport = new ExtentReports();
			String dir = System.getProperty("user.dir");
			WiniumUtils.criarDiretorio(dir + "\\report");
			limpaDiretorios();
			setDiretorioReport("./report/");
			WiniumUtils.criarDiretorio(diretorioReport);
			htmlReporter = new ExtentHtmlReporter(diretorioReport + "\\report.html");
			extentReport.attachReporter(htmlReporter);
		}
		extentTest = extentReport.createTest(cenario.getName());
	}

	public static void atualizaReport(Scenario cenario) {
		if (cenario.isFailed()) {
			extentTest.log(Status.ERROR, "Erro encontrado durante a execução.");
		} else {
			extentTest.log(Status.PASS, "Cenário executado com sucesso.");
		}
		extentReport.flush();
	}

	public static ExtentTest getExtentTest() {
		return extentTest;
	}

	public static void setDiretorioReport(String diretorio) {
		diretorioReport = diretorio;
	}

	public static String getDiretorioReport() {
		return diretorioReport;
	}

	public static void logMensagem(Status status, String mensagem, String imagem) {
		try {
			extentTest.log(status, mensagem, MediaEntityBuilder.createScreenCaptureFromPath(imagem).build());
			extentReport.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logMensagem(Status status, String mensagem) {
		extentTest.log(status, mensagem);
		extentReport.flush();
	}
	
	public static void limpaDiretorios() {
		File report = new File(System.getProperty("user.dir") + "\\report");
		deletarArquivo(report);
		File screenshots = new File(System.getProperty("user.dir") + "\\report\\screenshots");
		deletarArquivo(screenshots);
	}
}
