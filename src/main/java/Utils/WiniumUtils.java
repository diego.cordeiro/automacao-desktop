package Utils;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;

import Setups.TestRule;

public class WiniumUtils {
	public static WebDriver driver;
	protected static WebDriverWait wait;
	public static String nameCurrentScenario = TestRule.nomeCenario;

	public WiniumUtils() {
		driver = TestRule.getDriver();
	}

	public static String setValorArquivoProperties(String propertieFileName, String propertLoad) {
		Properties prop = new Properties();
		InputStream input = null;
		String path;
		if (usingJarFile()) {
			path = "";
		} else {
			path = "properties/";
		}
		String property = "";
		try {
			input = new FileInputStream(path + propertieFileName);
			prop.load(input);
			property = prop.getProperty(propertLoad);
		} catch (IOException ex) {
			ReportUtils.logMensagem(Status.FAIL, "Arquivo de properties não encontrado. " + ex.getMessage());
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					ReportUtils.logMensagem(Status.FAIL, "Arquivo de properties não encontrado. " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
		return property;
	}

	public static Boolean usingJarFile() {
		boolean isjar;
		String runningJarName = new WiniumUtils().getRunningJarName();
		if (runningJarName != null) {
			ReportUtils.logMensagem(Status.INFO, "Rodando testes via Jar : " + runningJarName);
			isjar = true;
		} else {
			// ReportUtils.logMensagem(Status.INFO, "Rodando testes pelo eclipse.");
			isjar = false;
		}
		return isjar;
	}

	public String getRunningJarName() {
		String className = this.getClass().getName().replace('.', '/');
		String classJar = this.getClass().getResource("/" + className + ".class").toString();
		if (classJar.startsWith("jar:")) {
			String vals[] = classJar.split("/");
			for (String val : vals) {
				if (val.contains("!")) {
					return val.substring(0, val.length() - 1);
				}
			}
		}
		return null;
	}

	public String somenteDigitos(String palavra) {
		String digitos = "";
		try {
			char[] letras = palavra.toCharArray();
			for (char letra : letras) {
				if (Character.isDigit(letra)) {
					digitos += letra;
				}
			}
		} catch (Exception e) {
			ReportUtils.logMensagem(Status.FAIL, "" + e.getMessage());
		}
		return digitos;
	}

	public static String removerAcentos(String str) {
		return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}

	public static String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static String formataData(String data) throws ParseException {
		data = data.substring(0, 10);
		String dataArray[] = data.split(Pattern.quote("-"));

		return dataArray[2] + "/" + dataArray[1] + "/" + dataArray[0];
	}

	public String formataDataHora(String dataHora) {
		String data = dataHora.substring(0, 10);
		String dataArray[] = data.split(Pattern.quote("-"));
		data = dataArray[2] + "/" + dataArray[1] + "/" + dataArray[0];

		String hora = dataHora.substring(11, 16);

		String dataHoraFormatadas = data + " " + hora;

		return dataHoraFormatadas;
	}

	public static String capturaTelaParaRelatorio() throws Exception {
		WebElement el = driver.findElement(By.className(setValorArquivoProperties("ambientes.properties", "PROGRAMA_NOME_JANELA")));
		Robot robot = new Robot();
		
		String areaPrint = el.getAttribute("BoundingRectangle").toString();
		String nomeImagemCaptura =  new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()).replace(".", "_") + "_captura_de_tela";
		String caminho_imagem_diretorio = new File("./report/screenshots/" + nomeImagemCaptura + ".png").getCanonicalPath();
		String arrayAreaPrint[] = areaPrint.split(",");
	
		Rectangle screenRectangle = new Rectangle(
				Integer.parseInt(arrayAreaPrint[0]), 
				Integer.parseInt(arrayAreaPrint[1]), 
				Integer.parseInt(arrayAreaPrint[2]),
				Integer.parseInt(arrayAreaPrint[3]));
		
		criarDiretorio(ReportUtils.getDiretorioReport() + "/screenshots");
		
		BufferedImage image = robot.createScreenCapture(screenRectangle);
		ImageIO.write(image, "png", new File(caminho_imagem_diretorio));
		
		return "./screenshots/" + nomeImagemCaptura + ".png";
	}

	public static void criarDiretorio(String diretorioASerCriado) {
		try {
			File diretorio = new File(diretorioASerCriado);
			if (!diretorio.exists()) {
				diretorio.mkdirs();
			}
		} catch (Exception e) {
			ReportUtils.logMensagem(Status.FAIL, "" + e.getMessage());
		}
	}

	public static void deletarArquivo(File arq) {
		if (arq.isDirectory()) {
			File[] arquivos = arq.listFiles();
			for (int i = 0; i < arquivos.length; i++) {
				deletarArquivo(arquivos[i]);
			}
		}
		arq.delete();
	}
	
	public static void finalizaProcesso(String executavel) throws InterruptedException, IOException{
		Process process = Runtime.getRuntime().exec("taskkill /F /IM " + executavel);
		process.waitFor();
		process.destroy();
	}
}